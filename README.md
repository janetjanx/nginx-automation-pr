# nginx automation with ansible and Docker

### Presentation

[Janet and Moses's Presentation](https://docs.google.com/presentation/d/1RbztlWpNBWnk2AQAuqC8eVXhFlteRAA3GUGaMuDFld0/edit?usp=sharing)

### To Get Started

```
git clone https://gitlab.com/janetjanx/nginx-automation-pr.git

cd nginx-automation-pr/

ansible-playbook playbook.yml -i inventory
```

